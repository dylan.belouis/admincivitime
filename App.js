import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Challenge from './components/Challenge/Challenge';
import SignUp from "./components/SignUp/SignUp";
import Login from "./components/Login/Login";
import Home from "./components/Home/Home";
import { AuthProvider } from "./components/Auth";
import PrivateRoute from "./components/PrivateRoute";
import NoMatch from "./components/NoMatch";
import User from "./components/Home/User/User";
import Chat from "./components/Home/Chat/Chat";
import Batiment from "./components/Batiment/Building";
import ViewBuilding from "./components/Batiment/personnalBuilding/PersonnalBuildingPage";
import CreateBuilding from "./components/Batiment/CreateBuilding";
import CreateImageBuilding from "./components/Batiment/personnalBuilding/CreateImageBuilding";
import EditBuilding from "./components/Batiment/personnalBuilding/EditBuilding";
import AddJson from "./components/Batiment/personnalBuilding/AddJson";
import Map from "./components/Home/Map/Map";
import Quiz from "./components/Home/Quiz/Quiz";
import QuizEdit from "./components/Home/Quiz/QuizEdit";
import QuizCreate from "./components/Home/Quiz/QuizCreate";
import Translation from "./components/Translation/Translation";
import TranslationsModal from "./components/Translation/TranslationsModal";

const App = () => {
    return (
        <AuthProvider>
            <Router>
                <Switch>
                    <PrivateRoute exact path="/" component={Home}/>
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/sign-up" component={SignUp} />
                    <PrivateRoute exact path="/user" component={User} />
                    <PrivateRoute exact path="/chat/:userId" component={Chat} />
                    <Route exact path="/challenge" component={Challenge} />
                    <Route exact path="/batiment" component={Batiment} />
                    <Route exact path="/batiment/create" component={CreateBuilding} />
                    <Route exact path="/batiment/:id" component={ViewBuilding} />
                    <Route exact path="/batiment/edit/:id/:key/:valid" component={EditBuilding}/>
                    <Route exact path="/batiment/:id/create" component={CreateImageBuilding} />
                    <Route exact path="/batiment/addjson/:id" component={AddJson} />
                    <PrivateRoute exact path="/challenge" component={Challenge} />
                    <PrivateRoute exact path="/map" component={Map} />
                    <PrivateRoute exact path="/quiz" component={Quiz} />
                    <PrivateRoute exact path="/quiz/edit/:idQuestion" component={QuizEdit} />
                    <PrivateRoute exact path="/quiz/create" component={QuizCreate} />
                    <Route exact path="/translation" component={Translation} />
                    <Route exact path="/translation/edit/:currentSection/:currentPath" component={TranslationsModal} />
                    <Route component={NoMatch} />
                    params
                </Switch>
            </Router>
        </AuthProvider>
    );
};

export default App;
