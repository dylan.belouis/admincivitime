import React, {useEffect, useState} from 'react';
import app from "../../../firebase";
import "./User.css";

import SidePanel from "../SidePanel/SidePanel";
import {
    Container,
    Paper,
    InputBase,
    FormControlLabel,
    RadioGroup, Radio,
    FormControl, FormLabel, IconButton, makeStyles
} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import LoopRoundedIcon from '@material-ui/icons/LoopRounded';
import TableUsers from "./TableUsers/TableUsers";

const useStyles = makeStyles({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    form_invisible: {
        display: 'none'
    },
    form_visible: {
        display: 'block',
        paddingLeft: '30px',
        marginTop: '25px'
    },
    input: {
        marginLeft: '15px',
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    Container_form_table : {
        width: '100%',
        display: 'flex'
    }
});

const User = () => {
    const classes = useStyles();
    const [listUsers, setListUsers] = useState([]);
    const [filterName, setFilterName] = useState("");
    const [typeFilter, setTypeFilter] = useState("name");

    const [displayFilter, setDisplayFilter] = useState(false);

    useEffect(() => {
        fetchListUser();
        handleSearch();
    }, [filterName]
    );

    const fetchListUser = () => {
        try {
            const usersRef = app.database().ref('users');
            usersRef.on("value", snapshot => {
                setListUsers(Object.values(snapshot.val()));
            });
        } catch (error) {
            console.log(error)
        }

    };

    const handleSearch = () => {
        if (filterName.length !== 0 && typeFilter === 'id') {
            const usersRef = app.database().ref('users');
            usersRef.orderByChild(typeFilter === 'id' && 'idUser')
                .equalTo(typeFilter === 'id' && parseInt(filterName))
                .on("value", snapshot => {
                    try {
                        setListUsers(Object.values(snapshot.val()));
                    } catch (err) {
                        console.log(err)
                    }
                });
        } else if (filterName.length !== 0 && typeFilter === 'name') {
            const usersRef = app.database().ref('users');
            usersRef.orderByChild('name')
                .on("value", snapshot => {
                    try {
                        const ListUsers = Object.values(snapshot.val());
                        const updateListUsers = ListUsers.filter((user) => user.name.includes(filterName));
                        setListUsers(updateListUsers)
                    } catch (err) {
                        console.log(err)
                    }
                });
        } else {
            console.log('faite une recherche')
        }

    };

    const handleChangeFilter = (e) => {
        setFilterName('');
        setTypeFilter(e.target.value)
    };

    return (
        <Container fixed>
            <SidePanel/>
            <div className={classes.Container_form_table}>
            <TableUsers listUsers={listUsers} displayFilter={displayFilter} setDisplayFilter={setDisplayFilter}/>
            <FormControl component="fieldset" className={!displayFilter ? classes.form_invisible : classes.form_visible}>
                <FormLabel component="legend">Filtrer :</FormLabel>
                <RadioGroup aria-label="position" name="position" value={typeFilter} onChange={handleChangeFilter} row>
                    <FormControlLabel
                        value="name"
                        control={<Radio color="primary"/>}
                        label="Par Nom"
                        labelPlacement="start"
                    />
                    <FormControlLabel
                        value="id"
                        control={<Radio color="primary"/>}
                        label="Par Id"
                        labelPlacement="start"
                    />
                </RadioGroup>
                <Paper component="form" className={displayFilter ? classes.root : classes.root_invisible}>
                    <InputBase
                        margin="dense"
                        type={typeFilter === 'name' ? 'text' : 'number'}
                        className={classes.input}
                        placeholder={typeFilter === 'name' ? "Recherche d'un utilisateur par son nom" : "Recherche d'un utilisateur par son id"}
                        inputProps={{'aria-label': 'search users'}}
                        value={filterName.toLowerCase()}
                        onChange={((e) => setFilterName(e.target.value))}
                    />
                    <IconButton type="button" className={classes.iconButton} aria-label="search"
                                onChange={handleSearch}>
                        <SearchIcon/>
                    </IconButton>

                    <IconButton type="button" onClick={fetchListUser}>
                        <LoopRoundedIcon/>
                    </IconButton>
                </Paper>
            </FormControl>
        </div>
        </Container>
    );
};


export default User;