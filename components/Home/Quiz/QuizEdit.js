import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import app from "../../../firebase";
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import {
    Container,
    Button,
    Box, FormControlLabel, Checkbox
} from "@material-ui/core";
import SidePanel from "../SidePanel/SidePanel";

import {Redirect} from "react-router";

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 600,
        },
    },
    btn_visible: {
        display: 'block',

    },
    btn_invisible: {
        display: 'none'
    },
    container_btn_answer: {
        display: 'flex',
    },
    title: {
        margin: '15px 0',
        textAlign: "center"
    },
    field_green: {
        border: "2px solid #10c710"
    },
    container_btn_save_del: {
        margin: '30px 10px',
        display: 'flex',
        justifyContent: 'space-evenly',
    }
}));

const QuizEdit = () => {
    const classes = useStyles();
    let {idQuestion} = useParams();
    const [oneQuestion, setOneQuestion] = useState(null);
    const [label, setLabel] = useState(null);
    const [explanation, setExplanation] = useState(null);
    const [answers, setAnswers] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);


    useEffect(() => {
        fetchQuestions()
    }, [idQuestion]);

    const fetchQuestions = () => {
        try {
            const idQuestionRef = app.database().ref(`quiz/questions/${idQuestion}`)
                .orderByKey()
                .limitToLast(100);
            idQuestionRef.on("value", snapshot => {
                if (snapshot.val() !== null) {
                    setOneQuestion(snapshot.val());
                    setAnswers(snapshot.val().answers);
                    setExplanation(snapshot.val().explanation);
                    setLabel(snapshot.val().label)
                } else {
                    console.log("ca a marché")
                }
            });
        } catch (error) {
            console.log(error)
        }
    };

    const updateCorrectAnswer = (e) => {
        const currentCorrectAnswer = (e.target.value);
        const result = [...answers];
        result[currentCorrectAnswer] = {
            ...result[currentCorrectAnswer],
            "isCorrect": true
        };
        setAnswers(result);
    };

    const handleDeleteFalseAnswer = (e) => {
        console.log(e.target.value)
        const falseAnswer = e.target.value;
        const result = [...answers];

        result[falseAnswer] = {
            ...result[falseAnswer],
            "isCorrect": false
        };
        setAnswers(result)
    };

    const handleChangeAnswer = (e, index) => {
        const inputVal = e.target.value;
        const result = [...answers];

        result[index] = {
            ...result[index],
            label: inputVal,
        };
        setAnswers(result)
    };

    const submitEdit = () => {
        app.database().ref(`quiz/questions/${idQuestion}`)
            .update({
                explanation,
                label,
                answers
            });
        setIsSubmit(true)
    };

    // Suppression d'un quiz
    const deleteQuiz = () => {
        app.database().ref(`/quiz/questions/${idQuestion}`).remove();
        setIsSubmit(true);
    };

    if (!oneQuestion) {
        return (
            <>
                {isSubmit && <Redirect to="/quiz"/>}
                <div>Loading</div>
            </>
        )
    }

    return (
        <Container fixed>
            {isSubmit && <Redirect to="/quiz"/>}
            <SidePanel/>
            <h3 className={classes.title}>Modifier une question existante</h3>
            <form className={classes.root} noValidate autoComplete="off">
                <div>
                    <TextField
                        multiline
                        rowsMax="6"
                        id="outlined-required"
                        label="Questions"
                        value={label}
                        variant="outlined"
                        onChange={(e) => setLabel(e.target.value)}
                    />
                    <TextField
                        id="outlined-required"
                        label="Explication"
                        multiline
                        rowsMax="6"
                        value={explanation}
                        variant="outlined"
                        onChange={(e) => setExplanation(e.target.value)}
                    />
                    {answers.map((answer, index) => (
                        <Box key={index} className={classes.container_btn_answer}>
                            <TextField
                                multiline
                                className={answer.isCorrect ? classes.field_green : ""}
                                rowsMax="4"
                                id="outlined-required"
                                label={`Réponses ${index + 1}`}
                                variant="filled"
                                value={answer.label}
                                onChange={(e) => handleChangeAnswer(e, index)}
                            />
                            <FormControlLabel
                                checked={answer.isCorrect === true}
                                value={index}
                                control={<Checkbox value={index}
                                                   onChange={answer.isCorrect ? handleDeleteFalseAnswer : updateCorrectAnswer}
                                                   color="primary"/>}
                                label=""
                                labelPlacement="start"
                            />
                        </Box>
                    ))}
                    <div className={classes.container_btn_save_del}>
                        <Button variant="contained" color="primary" onClick={submitEdit}> Sauvegarder
                            Changements</Button>
                        <Button variant="contained" color="secondary" onClick={deleteQuiz}> Supprimer Quiz</Button>
                    </div>
                </div>

            </form>
        </Container>
    )
};


export default QuizEdit
