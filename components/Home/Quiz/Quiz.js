import React, {useEffect, useState} from "react";
import QuizTable from "./QuizTable/QuizTable";
import SidePanel from "../SidePanel/SidePanel";
import {Container} from "@material-ui/core";
import app from "../../../firebase";

const Quiz = () => {
    const [listQuestions, setListQuestions] = useState([]);

    useEffect(() => {
        fetchQuestions()
    }, []);

    const fetchQuestions = () => {
        try {
        const refQuestions = app.database().ref('quiz/questions');
            refQuestions.on("value", snapshot => {
                if (snapshot.val() !== undefined) {
                    console.log(snapshot.val());
                    setListQuestions(Object.values(snapshot.val()));
                }
            });
        } catch (error) {
        console.log(error)
        }
    };

    return (
        <Container>
            <SidePanel/>
            {<QuizTable listQuestions={listQuestions} setListQuestions={setListQuestions}  />}
        </Container>
    )
};

export default Quiz