import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import {
    Box,
    Button, Checkbox,
    Container, FormControlLabel,
} from "@material-ui/core";

import {makeStyles} from "@material-ui/core/styles";
import app from "../../../firebase";
import SidePanel from "../SidePanel/SidePanel";
import {Redirect} from "react-router";

const nanoId = require('nanoid');

const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '100%',
        },
    },
    title: {
        margin: '15px 0',
        textAlign: "center"
    },
    container_btn_add_remove: {
        margin: '30px 10px',
        display: 'flex',
        justifyContent: 'space-evenly',
    },
    container_btn_answer: {
        display: 'flex',
    },
    field_green: {
        border: "1px solid #10c710"
    },
    container_btn_saveChange: {
        display: 'flex',
        justifyContent: 'center',
        margin: '25px 0'
    }
}));

const QuizCreate = () => {
    const classes = useStyles();
    const [label, setLabel] = useState("");
    const [explanation, setExplanation] = useState("");
    const [numberAnswers, setNumberAnswers] = React.useState(4);
    const [answers, setAnswers] = useState([]);
    const [isSubmit, setIsSubmit] = useState(false)


    const changeValueAnswer = (e, index) => {
        const inputVal = e.target.value;
        const result = [...answers];
        result[index] = {
            ...result[index],
            label: inputVal
        };
        setAnswers(result)
    };

    const generateNumberAnswer = (number) => {
        let answersField = [];
        for (let index = 0; index < number; index++) {
            answersField.push(
                <Box className={classes.container_btn_answer}>
                    <TextField
                        className={handleChangeClassNameField(index)}
                        key={index}
                        multiline
                        rowsMax="4"
                        id="outlined-required"
                        label={`Réponses ${index + 1}`}
                        variant="filled"
                        value={answers.label}
                        onChange={(e) => changeValueAnswer(e, index)}
                    />
                    <FormControlLabel
                        checked={handleChangeChecked(index)}
                        value={index}
                        control={<Checkbox value={index}
                                           //onChange={answers.isCorrect ? handleDeleteFalseAnswer : updateGoodAnswer}
                                            onChange={(e) => handleChangeAnswer(e,index)}
                                           color="primary"/>}
                        label=""
                        labelPlacement="start"
                    />
                </Box>
            );
        }
        return answersField
    };

    const handleChangeAnswer = (e,index) => {
        if (answers[index]) {
            if (answers[index].isCorrect) {
                return handleDeleteFalseAnswer(e)
            }
            return updateGoodAnswer(e)
        } else {
            return updateGoodAnswer(e)
        }
    };

    const updateGoodAnswer = (e) => {
        console.log('update good réponse')
        const currentCorrectAnswer = (e.target.value);
        const result = [...answers];
        result[currentCorrectAnswer] = {
            ...result[currentCorrectAnswer],
            "isCorrect": true
        };
        setAnswers(result)
    };

    const handleDeleteFalseAnswer = (e) => {
        console.log('update false réponse')
        console.log(e.target.value);
        const falseAnswer = e.target.value;
        const result = [...answers];

        result[falseAnswer] = {
            ...result[falseAnswer],
            "isCorrect": false
        };
        setAnswers(result)
    };

    const handleChangeClassNameField = (index) => {
        if (answers[index]) {
            if (answers[index].isCorrect) {
                return classes.field_green
            }
            return ""
        } else {
            return ""
        }
    };

    const handleChangeChecked = (index) => {
        if (answers[index]) {
            if (answers[index].isCorrect) {
                return true
            }
            return false
        } else {
            return false
        }
    };



    const submitQuiz = () => {
        const idQuestion = nanoId();
        app.database().ref("quiz/questions").child(idQuestion).set({
            label,
            explanation,
            answers,
            idQuestion
        });
        setIsSubmit(true)
    };

    return (
        <Container fixed>
            {isSubmit && <Redirect to="/quiz"/>}
            <SidePanel/>
            <h3 className={classes.title}>Creer une question</h3>
            <form className={classes.root} noValidate autoComplete="off">
                <div>
                    <TextField
                        multiline
                        rowsMax="6"
                        id="outlined-required"
                        label="Questions"
                        value={label}
                        variant="outlined"
                        onChange={(e) => setLabel(e.target.value)}
                    />
                    <TextField
                        id="outlined-required"
                        label="Explication"
                        multiline
                        rowsMax="6"
                        value={explanation}
                        variant="outlined"
                        onChange={(e) => setExplanation(e.target.value)}
                    />
                </div>

                <div className={classes.container_btn_add_remove}>
                    <Button variant="contained" color="primary"
                            onClick={() => setNumberAnswers(numberAnswers + 1)}> Ajouter Réponse</Button>
                    <Button variant="contained" color="secondary"
                            onClick={() => numberAnswers > 2 && setNumberAnswers(numberAnswers - 1)}> Retirer Réponse
                    </Button>
                </div>
                {generateNumberAnswer(numberAnswers)}
                <div className={classes.container_btn_saveChange}>
                    <Button variant="contained" color="primary" onClick={submitQuiz}> sauvegarder changements</Button>
                </div>
            </form>
        </Container>
    )
};

export default QuizCreate
