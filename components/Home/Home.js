import React, { useContext } from "react";

import { AuthContext } from "../Auth";
import SidePanel from "./SidePanel/SidePanel";
import {Container} from "@material-ui/core";

const Home = () => {
    const currentUser  = useContext(AuthContext);

    return (
            <Container fixed>
                <SidePanel/>
                <h1>Home</h1>
                {currentUser && <p>{`Connecter en tant que : ${currentUser.email}`}</p>}
            </Container>
    );
};

export default Home;