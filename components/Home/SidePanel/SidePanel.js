import React, {useState} from 'react';
import {Link} from "react-router-dom";

// Import Components of Material-Ui
import {Drawer, Button, List, Divider, ListItem, ListItemText} from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import MapIcon from '@material-ui/icons/Map';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import PeopleIcon from '@material-ui/icons/People';
import HomeIcon from '@material-ui/icons/Home';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import LocationCityIcon from '@material-ui/icons/LocationCity';
import TranslateIcon from '@material-ui/icons/Translate';
import app from "../../../firebase";
import {makeStyles} from '@material-ui/core/styles';

import "./SidePanel.css"

const useStyles = makeStyles({
    list: {
        width: 250,
    },
    listItem: {
        color: 'black',
        textDecoration: 'none'
    }
});

const SidePanel = () => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const toggleDrawer = (open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setOpen(open);
    };

    const sideList = () => (
        <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
        >
            <List>
                <Link to="/">
                    <ListItem button>
                        <ListItemIcon><HomeIcon/></ListItemIcon>
                        <ListItemText className={classes.listItem} primary="Home"/>
                    </ListItem>
                </Link>
                <Link to="/user">
                    <ListItem button>
                        <ListItemIcon><PeopleIcon/></ListItemIcon>
                        <ListItemText className={classes.listItem} primary="Users"/>
                    </ListItem>
                </Link>
                <Link to="/challenge">
                    <ListItem button>
                        <ListItemIcon><VerifiedUserIcon/></ListItemIcon>
                        <ListItemText className={classes.listItem} primary="Challenges"/>
                    </ListItem>
                </Link>
                <Link to="/map">
                    <ListItem button>
                        <ListItemIcon><MapIcon/></ListItemIcon>
                        <ListItemText className={classes.listItem} primary="Map"/>
                    </ListItem>
                </Link><Link to="/quiz">
                <ListItem button>
                    <ListItemIcon><ContactSupportIcon/></ListItemIcon>
                    <ListItemText className={classes.listItem} primary="Quiz"/>
                </ListItem>
            </Link>
                <Link to="/translation">
                    <ListItem button>
                        <ListItemIcon><TranslateIcon/></ListItemIcon>
                        <ListItemText className={classes.listItem} primary="Traduction"/>
                    </ListItem>
                </Link>
                <Link to="/batiment">
                    <ListItem button>
                        <ListItemIcon><LocationCityIcon/></ListItemIcon>
                        <ListItemText className={classes.listItem} primary="Batiment" />
                    </ListItem>
                </Link>
                <Divider/>
                <ListItem button onClick={() => app.auth().signOut()}>
                    <ListItemIcon><ExitToAppIcon/></ListItemIcon>
                    <ListItemText primary="Se Déconnecter"/>
                </ListItem>
            </List>
        </div>
    );

    return (
        <div className="button_sidePanel">
            <Button variant="outlined" color="primary" onClick={toggleDrawer(true)}>Open Menu</Button>
            <Drawer open={open} onClose={toggleDrawer(false)}>
                {sideList()}
            </Drawer>
        </div>
    );
};

export default SidePanel;
