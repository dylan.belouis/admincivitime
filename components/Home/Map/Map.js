import React, {useEffect, useState} from "react";
import app from "../../../firebase";
import useStyles from "./Style";
import {
    Container,
    Input,
    CardContent,
    CardActionArea,
    Typography,
    Card,
    CardMedia,
    Fab,
    FormGroup,
    TextField,
    Box,
    Button,
    GridList,
    GridListTile,
    GridListTileBar,
    ListSubheader,
    CircularProgress,
    MenuItem,
    FormControl,
    Select,
    InputLabel
} from "@material-ui/core";

import {Alert} from '@material-ui/lab';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import SidePanel from "../SidePanel/SidePanel";

const nanoId = require('nanoid');

const Map = () => {
    const classes = useStyles();

    const [currentMap, setCurrentMap] = useState("");
    const [label, setLabel] = useState("");
    const [currentFile, setCurrentFile] = useState(null);
    const [valueInputMissing, setValueInputMissing] = useState(true);
    const [allMap, setAllMap] = useState(null);
    const [currentKey, setCurrentKey] = useState("");
    const [modeEdit, setModeEdit] = useState(false);

    // SELECT
    const [hiddenMap, setHiddenMap] = React.useState('');
    const [open, setOpen] = React.useState(false);


    useEffect(() => {
            // GET ALL MAP TO DISPLAY THEM
            const fetchAllMap = () => {
                try {
                    const allMapRef = app.database().ref(`map`)
                        .orderByKey()
                        .limitToLast(100);
                    allMapRef.on('value', snapshot => {
                        if (snapshot.val() !== null) {
                            setAllMap(Object.values(snapshot.val()));
                        }
                    });
                    if (!label || !currentFile) {
                        setValueInputMissing(true)
                    } else {
                        setValueInputMissing(false)
                    }
                } catch (error) {
                    console.log(error)
                }
            };

            fetchAllMap()


        }, [label, currentFile]
    );


    // GET LABEL AND FILE OF ONE MAP FOR PREVIEW
    const editMap = (key) => {
        app.database().ref("map/" + key).on('value', function (snapshot) {
            setLabel(snapshot.val().label);
            setCurrentMap(snapshot.val().location);
        });
        setCurrentFile(null);
        setCurrentKey(key);
        setModeEdit(true)
    };

    const handleHiddenMap = (key) => {
        app.database().ref(`map/`).child(key)
            .update({
                hidden: true
            })
    };

    const handleShowMap = (key) => {
        app.database().ref(`map/`).child(key)
            .update({
                hidden: false
            })
    };

    // GET PREVIEW OF INPUT FILE
    const PreviewFile = (e) => {
        const file = e.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = (event) => {
            setCurrentMap(() => event.target.result)
        };
        setCurrentFile(file);
    };

    // EDIT FILE AND/OR LABEL, CREATE FILE IN STORAGE AND CHANGE URL FROM DATABASE
    const validateEdit = () => {
        const idEditMap = nanoId();
        if (currentFile) {
            const uploadTask = app.storage().ref('map/' + idEditMap).put(currentFile);
            uploadTask.on(`state_changed`,
                (snapshot) => {
                    console.log(snapshot)
                },
                (error) => {
                    console.log(error)
                },
                () => {
                    app.storage().ref('map').child(idEditMap).getDownloadURL()
                        .then(url => {
                            app.database().ref(`map/`).child(currentKey)
                                .update({
                                    label,
                                    location: url
                                })
                        })
                });
        } else {
            app.database().ref(`map/`).child(currentKey)
                .update({
                    label,
                });
        }
    };

    // UPLOAD FILE INPUT TO STORAGE AND GET URL FOR DATABASE
    const uploadFile = () => {
        if (currentFile && label) {
            try {

                const idMap = nanoId();
                const uploadTask = app.storage().ref(`map/${idMap}`).put(currentFile);
                uploadTask.on(`state_changed`,
                    (snapshot) => {
                        console.log(snapshot)
                    },
                    (error) => {
                        console.log(error)
                    },
                    () => {
                        app.storage().ref('map').child(idMap).getDownloadURL()
                            .then(url => {
                                app.database().ref(`map/`).child(idMap)
                                    .set({
                                        label,
                                        location: url,
                                        idMap,
                                        hidden: false
                                    });
                            })
                    });
            } catch (err) {
                console.log(err)
            }
            setCurrentMap("");
            setLabel("");
            setCurrentFile(null);
        } else {
            console.log("les conditions ne sont pas respecté")
        }
    };

    const alertCondition = () => {
        if (valueInputMissing && !modeEdit) {
            return <Alert severity="error">Veuillez choisir une map et lui attribuer un titre!</Alert>
        } else if (!valueInputMissing && !modeEdit) {
            return <Alert severity="success">`Super, vous pouvez envoyer votre map avec comme le label
                suivant: {label}`</Alert>
        } else if (valueInputMissing && modeEdit) {
            return <Alert severity="error">En mode edit, vous pouvez modifier le label et/ou la Carte !</Alert>
        } else if (valueInputMissing && modeEdit) {
            return <Alert severity="success">`Super, vous pouvez envoyer votre map modifé avec le label
                suivant: {label}`</Alert>
        } else {
            console.log("else")
        }
    };


    return (
        <Container fixed>
            <SidePanel/>
            <h3 className={classes.title}>Consulter, Ajouter et Modifier les cartes</h3>
            {alertCondition()}
            <FormGroup>
                <TextField className={classes.form_textField} margin='dense'
                           error={label === "Changez le Titre de la Carte" || label === ""} required
                           id="component-error" label="Titre de la carte" variant="outlined" value={label}
                           onChange={(e) => setLabel(e.target.value)}/>
                <Input className={classes.form_input} type="file" margin='dense' required onChange={PreviewFile}/>
            </FormGroup>

            <Card className={classes.root}>
                <CardActionArea
                    className={currentFile || modeEdit ? classes.card_media_container_visible : classes.card_media_container_invisible}>
                    <CardMedia
                        className={classes.media}
                        title="Contemplative Reptile"
                        alt="carte choisis"
                    ><img className={classes.media_map} src={currentMap} alt="map current"/></CardMedia>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {label}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
            <Box className={classes.float_buttons_container}>
                {!modeEdit ? <Fab color="primary" aria-label="add" disabled={!(currentFile && label)}
                                  onClick={uploadFile}><AddIcon/></Fab> :
                    <>
                        <Fab onClick={validateEdit} color="secondary" aria-label="edit"><EditIcon/></Fab>
                        <Button
                            onClick={() => setModeEdit(false) || setCurrentMap("") || setLabel("") || setCurrentFile(null)}
                            color="primary" aria-label="edit">passez en mode creer</Button>
                    </>}
            </Box>

            <div>
                <FormControl className={classes.formControl}>
                    <InputLabel id="demo-controlled-open-select-label">Trier</InputLabel>
                    <Select
                        labelId="demo-controlled-open-select-label"
                        id="demo-controlled-open-select"
                        open={open}
                        onClose={() => setOpen(false)}
                        onOpen={() => setOpen(true)}
                        value={hiddenMap}
                        onChange={(e) => setHiddenMap(e.target.value)}
                    >
                        <MenuItem value={false}>Cartes Visibles</MenuItem>
                        <MenuItem value={true}>Cartes Visible + Caché</MenuItem>
                    </Select>
                </FormControl>
            </div>

            {!allMap ? <CircularProgress disableShrink/> :
                <div className={classes.root}>
                    <GridList cellHeight={180} className={classes.gridList}>
                        <GridListTile key="Subheader" cols={2} style={{height: 'auto'}}>
                            <ListSubheader
                                component="div">{hiddenMap ? "*Voici toutes les Cartes visibles y compris celle caché" : "*Voici seulement les Cartes visibles"}</ListSubheader>
                        </GridListTile>
                        {allMap.map(map => (
                            <GridListTile key={map.idMap}
                                          className={map.hidden && !hiddenMap ? classes.miniature_map_hidden : classes.miniature_map}>
                                <a href={map.location} target="_blank" rel="noopener noreferrer">
                                    <img src={map.location}
                                         className={map.hidden ? classes.miniature_map_img_invisible : classes.miniature_map_img_visible}
                                         alt={map.label}/>
                                </a>
                                <GridListTileBar
                                    title={map.label}
                                    actionIcon={
                                        <>
                                            {hiddenMap && map.hidden ?
                                                <Button variant="contained" color="primary"
                                                        onClick={() => handleShowMap(map.idMap)}>Rendre Visible</Button>
                                                :
                                                <Button variant="contained" onClick={() => handleHiddenMap(map.idMap)}>Rendre
                                                    Caché</Button>
                                            }


                                            <Button variant="contained" color="secondary"
                                                    onClick={() => editMap(map.idMap)}>Edit</Button>
                                        </>
                                    }
                                />
                            </GridListTile>
                        ))}
                    </GridList>
                </div>}
        </Container>
    )
};

export default Map;
