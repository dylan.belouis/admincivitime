import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    root: {
        minWidth: 245,
    },
    media: {
        minHeight: 440,
        backgroundSize: 'contain',
    },
    card_media_container_invisible: {
        display: 'none'
    },
    card_media_container_visible: {
        display: 'block'
    },
    miniature_map_hidden: {
        display: 'none'
    },
    miniature_map: {
        display: 'block'
    },
    miniature_map_img_visible: {
        width: '100%',
    },
    miniature_map_img_invisible: {
        width: '100%',
        filter: 'grayscale(0.9)',
    },
    media_map: {
        width: '100%',
        maxHeight: '60vh',
    },
    title: {
        margin: '15px 0',
        textAlign: "center"
    },
    form_textField: {
        margin: '15px 0 5px 0',
    },
    form_input: {
        margin: '15px 0',
    },
    float_buttons_container: {
        padding: "15px",
        display: "flex",
        justifyContent: "space-between"
    },
    button: {
        display: 'block',
    },
    formControl: {
        minWidth: 120,
    },
});

export default useStyles;