import React, {useContext, useEffect, useState} from 'react';
import app  from '../../../firebase';
import './Chat.css';
import {Container, TextField, Button} from "@material-ui/core";
import { useParams } from "react-router-dom";
import { AuthContext } from "../../Auth";
import SidePanel from "../SidePanel/SidePanel";
const moment = require('moment');

const Chat = () => {
    const currentUser  = useContext(AuthContext);
    const [ messages, setMessages ] = useState([]);
    const [ currentMessage, setCurrentMessage ] = useState("");
    const [ chatUser, setChatUser ] = useState("");
    let { userId } = useParams();

    useEffect(() => {
        const messagesRef = app.database().ref(`users/${userId}/messages`)
            .orderByKey()
            .limitToLast(100);
        messagesRef.on('value', snapshot => {
            setMessages(Object.values(snapshot.val()));
        });

        const chatUserRef = app.database().ref(`users/${userId}/name`);
        chatUserRef.on('value', snapshot => {
            setChatUser(snapshot.val());
        });

    }, [userId]);

    const onAddMessage = (event) => {
        event.preventDefault();
        console.log("reponses donné");
        app.database().ref(`users/${userId}/messages`).push({text: currentMessage, idSender: currentUser.uid, date: moment().format('HH:mm:ss')});
        setCurrentMessage("");
    };

        return (
            <Container fixed >
                <SidePanel />
                <h3 className="chat_title-name">{!chatUser ? "Chargement du Tchat " : `Chat avec ${chatUser}`}</h3>
                <div className="chat__container">
                    {messages && messages.map((message, index) =>  {
                        return (
                            <div className={message.idSender === currentUser.uid ? "msg_admin" : "msg_user"} key={index}>
                                <div className="chat__block-msg">
                                    <code className="chat__block-date">{message.date}</code>
                                    <p className="">{message.text}</p><span>{message.idSender === currentUser.uid && "Moi"}</span>
                                </div>
                            </div>
                            )
                    })}
                </div>
                <div >
                    <TextField
                        id="outlined-full-width"
                        value={currentMessage}
                        onChange={(e) => setCurrentMessage(e.target.value)}
                        label="Label"
                        style={{ margin: 8 }}
                        placeholder="entrer votre message"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                    />
                    <Button className="" variant="contained" color="primary" onClick={onAddMessage}>Envoyer</Button>
                </div>
            </Container>
        );
};

export default Chat;