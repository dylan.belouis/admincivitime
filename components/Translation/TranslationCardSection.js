import React from "react";
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    rootValidate: {
        maxWidth: 345,
        border: "2px solid #10c710"
    },
    rootInvalidate: {
        maxWidth: 345,
    },
    translation_container_cards: {
        display: "flex",
        justifyContent: "space-evenly",
        margin: "40px 0"
    }
});

const TranslationCardSection = ({sections, currentSection, setCurrentSection}) => {
    const classes = useStyles();


    return (
        <div className={classes.translation_container_cards}>
            {sections && sections.map((section, index) => {
                return (
                    <Card className={section[0] === currentSection ? classes.rootValidate : classes.rootInvalidate } key={index} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt="Contemplative Reptile"
                                height="140"
                                image="https://media-exp1.licdn.com/dms/image/C561BAQGKroRdu5k4TQ/company-background_10000/0?e=2159024400&v=beta&t=RLmxhDpL7AfXdt2ixhuzakfgJaXw_PYEV1eajhQ72oM"
                                title="Contemplative Reptile"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {section[0]}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    traduire les texte de {section[0]}

                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary" value={section[0]} onClick={(e) => setCurrentSection(e.currentTarget.value)}>
                                Voir plus >
                            </Button>
                        </CardActions>
                    </Card>
                )
            })
            }
        </div>
    )
}

    export default TranslationCardSection
