import React, {useEffect, useState} from "react";
import {
    Container, InputLabel, Select, FormControl, MenuItem, Radio, RadioGroup, FormLabel, FormControlLabel
} from "@material-ui/core";
import SidePanel from "../Home/SidePanel/SidePanel";
import app from "../../firebase";
import "./Translation.css"
import TranslationTable from "./TranslationTable";
import TranslationCardSection from "./TranslationCardSection";
import useLocalStorage from 'react-use-localstorage';


const Translation = () => {
    const [translations, setTranslations] = useState([]);
    const [translationsDisplay, setTranslationsDisplay] = useState([]);

    const [sections, setSections] = useState([]);
    const [currentSection, setCurrentSection] = useLocalStorage("currentSection", null);
    const [open, setOpen] = useState(false);
    const [currentLang, setCurrentLang] = useState('fr');
    const [currentPath, setCurrentPath] = useState(null);
    const [chooseFilterTranslation, setChooseFilterTranslation] = useState("all");
    const [changeFilter, setChangeFilter] = useState(true);

    const langAvailable = ["fr", "en", "es", "ch"];


    useEffect(() => {
            checkTranslationAvailable();
            fetchTextUpdate()
        }, [translations, currentLang, currentSection]
    );
    useEffect(() => {
            fetchSection();
        }, []
    );

    const checkTranslationAvailable = async () => {
        const copyTranslations = [...translations]
        try {
            await copyTranslations.forEach((text, index) => {
                    const langMissing = [];
                    langAvailable.forEach((lang) => app.database().ref(`translation/${lang}/${currentSection}/${text.key.replace(/,/g, '/')}/`).orderByKey().on('value', snapshot => {
                        if (snapshot.val() === null || snapshot.val() === "") {
                            langMissing.push(lang);
                            copyTranslations[index] = {
                                ...copyTranslations[index],
                                "langMiss": langMissing,
                            }
                        } else {
                            copyTranslations[index] = {
                                ...copyTranslations[index],
                            }
                        }
                    }))
                }
            );
        } catch (error) {
            console.log(error)
        } finally {
            if (changeFilter && translations && copyTranslations) {
                if (chooseFilterTranslation === "onlyNoTrad") {
                    console.log("dans le second if");
                    setTranslationsDisplay(copyTranslations.filter((trad) => trad.langMiss));
                    setChangeFilter(false)
                } else if (chooseFilterTranslation === "all") {
                    console.log('dans le else');
                    setTranslationsDisplay(copyTranslations);
                    setChangeFilter(false)
                } else {
                    console.log('dans le else final')
                }
            }
        }
    };

    const fetchSection = () => {
        try {
            app.database().ref(`translation/${currentLang}`).orderByKey().limitToLast(100).once('value', snapshot => {
                if (snapshot.val() !== null) {
                    setSections((Object.entries(snapshot.val())));
                }
            });
        } catch (error) {
            console.log(error)
        }
    };

    const fetchTextUpdate = () => {
        try {
            app.database().ref(`translation/${currentLang}/${currentSection}`).orderByKey().limitToLast(100).once('value', snapshot => {
                if (snapshot.val() !== null) {
                    setTranslations(flattenTranslations((snapshot.val())));
                }
            });
        } catch (error) {
            console.log(error)
        }
    };

    const flattenTranslations = (obj, parents = []) => {
        if (typeof obj !== 'object') {
            return []
        }
        return Object.entries(obj)
            .flatMap(([currentItemName, value]) => {
                if (typeof value !== 'object') {
                    return [
                        {
                            key: parents.concat(currentItemName).toString(),
                            value,
                        },
                    ]
                }
                return flattenTranslations(value, parents.concat(currentItemName))
            })
    };

    const handleChangeFilter = (e) => {
        setChooseFilterTranslation(e.target.value);
        setChangeFilter(true)
    };


    return (
        <Container fixed>
            <SidePanel/>
            <div>
                <TranslationCardSection sections={sections} setCurrentSection={setCurrentSection}
                                        currentSection={currentSection}/>


            </div>

            {translations ?
            <>
                <FormControl>
                    <InputLabel id="demo-controlled-open-select-label">Langue</InputLabel>
                    <Select
                        labelId="demo-controlled-open-select-label"
                        id="demo-controlled-open-select"
                        open={open}
                        onClose={() => setOpen(false)}
                        onOpen={() => setOpen(true)}
                        value={currentLang}
                        onChange={(e) => setCurrentLang(e.target.value)}
                    >
                        <MenuItem value={"fr"}>Francais</MenuItem>
                        <MenuItem value={"en"}>Anglais</MenuItem>
                        <MenuItem value={"es"}>Espagnol</MenuItem>
                        <MenuItem value={"ch"}>Chinois</MenuItem>
                    </Select>
                </FormControl>

                <FormControl component="fieldset">
                    <FormLabel component="legend">Filtre traduction</FormLabel>
                    <RadioGroup aria-label="gender" name="gender1" value={chooseFilterTranslation}
                                onChange={handleChangeFilter}>
                        <FormControlLabel value="all" control={<Radio/>} label="tout"/>
                        <FormControlLabel value="onlyNoTrad" control={<Radio/>} label="Seulement les non Traduite"/>
                    </RadioGroup>
                </FormControl>

                <TranslationTable translationsDisplay={translationsDisplay} currentPath={currentPath}
                                  setCurrentPath={setCurrentPath}
                                  currentSection={currentSection} currentLang={currentLang}
                                  checkTranslationAvailable={checkTranslationAvailable}/>
            </>
            :
            <p>choose a section</p>}
        </Container>
    )
};

export default Translation
