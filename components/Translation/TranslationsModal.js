import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import { Button, Container} from "@material-ui/core";
import app from "../../firebase";
import TextField from "@material-ui/core/TextField";
import {useParams} from "react-router-dom";
import {Redirect} from "react-router";


const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 600,
        },
    },
}));

export default function TranslationsModal() {
    const classes = useStyles();
    let {currentPath, currentSection} = useParams();

    const [textSelected, setTextSelected] = useState([]);
    const [langAvailable, setLangAvailable] = useState(["fr", "en", "es", "ch"]);
    const [isSubmit, setIsSubmit] = useState(false);



    useEffect(() => {
        if (currentPath) {
            fetchTextSelected();
        }
    }, [currentPath, setTextSelected]);


    function fetchTextSelected() {
        console.log("fetch selected")
        try {
            setTextSelected([])
            langAvailable.map((lang, index) => (
                app.database().ref(`translation/${lang}/${currentSection}/${currentPath.replace(/,/g, '/')}/`).orderByKey().on('value', snapshot => {
                    if (snapshot.val() !== null) {
                        setTextSelected(result => {
                            const intermediate = [...result];
                            const salut = currentPath.split(",").pop()
                            intermediate[index] = {
                                ...intermediate[index],
                                "value": snapshot.val(),
                                "lang": lang,
                                "label": salut
                            };
                            return intermediate
                        })
                    } else {
                        setTextSelected(result => {
                            const intermediate = [...result];
                            intermediate[index] = {
                                ...intermediate[index],
                                "value": "pas de traduction",
                                "lang": lang,
                                "label": currentPath.split("/").pop()
                            };
                            return intermediate
                        })
                    }
                })


            ));
        } catch (error) {
            console.log(error)
        }
    }


    const handleChangeText = (e, index) => {
        let inputVal = e.target.value;
        let result = [...textSelected];

        result[index] = {
            ...result[index],
            value: inputVal,
        };
        setTextSelected(result)
    };

    const submitEdit = (e, index, lang) => {
        handleChangeText(e, index)
        const inputVal = textSelected[index].value
        const path = currentPath.replace(/,/g, '/');

        app.database().ref(`translation/${lang}/${currentSection}`)
            .update({
              [path] : inputVal
            });

        setIsSubmit(true)

    };


    return (
        <Container fixed>
            {isSubmit && <Redirect to="/translation"/>}
            <div className={classes.paper}>
                {textSelected && textSelected.map((trad, index) => (
                    <form className={classes.root} key={index} noValidate autoComplete="off">
                        {index === 0 && <h2>{textSelected[0].label}</h2>}
                        <TextField
                            label={trad.lang}
                            multiline
                            rowsMax="6"
                            value={trad.value}
                            variant="filled"
                            onChange={(e) => handleChangeText(e, index)}
                        />
                        <Button variant="contained" color="secondary" value={trad.value}
                                onClick={(e) => submitEdit(e, index, trad.lang)}> Edit</Button>
                    </form>
                ))}
            </div>
        </Container>
    );
}
