import React, { useState } from 'react'
import {
    Container,
    Button,
    TextField,
    Snackbar
} from '@material-ui/core'
import { Link } from 'react-router-dom'
import useStyles from '../assets/Stylebatiment'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { useParams } from 'react-router'
import firebase from 'firebase'
import nanoid from 'nanoid'
import Alert from '@material-ui/lab/Alert'


const CreateBuilding = (props) => {

    const { id }    = useParams()
    const classes   = useStyles()
    const [errorForm, setErrorForm]       = useState(false)
    const [sendValid, setSendValid]       = useState(false)
    const [label, setLabel]               = useState('')
    const [lengthArray, setLengthArray]   = useState(0)
    let   arrayTest = []
    let   imageFile


    const handleChange = (e) => {
        if(e.target.files.length > 1) {
            for (let i = 0; i < e.target.files.length; i++) {
                imageFile = e.target.files[i]
                arrayTest.push(e.target.files[i])
                Object.values(arrayTest[i])
                setLengthArray(arrayTest.length)
                console.log(arrayTest)
            }
        } else {
            arrayTest = e.target.files[0]
        }
    }

        const createImage = () => {
            console.log(arrayTest)
            setErrorForm(true)
            if (lengthArray > 1 && label) {
                for (let i = 0; i < lengthArray; i++) {
                const newKey = nanoid()
                const metadata = { contentType: 'image/*' }
                console.log(imageFile[i])
                const uploadTask = firebase.storage().ref("Building/" + id + "/images/").child(arrayTest[i].name + '/').put(arrayTest[i], metadata)
                uploadTask.on(`state_changed`,
                (snapshot) => {
                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                    console.log('Upload is ' + progress + '% done')
                    console.log(snapshot)
                    setErrorForm(false)
                    setSendValid(true)
                },
                (error) => {
                    console.log(error)   
                    setErrorForm(true)
                    setSendValid(false)
                },
                // eslint-disable-next-line
                () => {
                    firebase.storage().ref(`Building/${id}/images/${arrayTest[i].name}`).getDownloadURL().then(url => {
                        firebase.database().ref("Building/" + id + "/images/").child(newKey)
                        .set({
                            label,
                            buildingUrl:url,
                            isValid: true
                        })
                        setSendValid(true)
                    })
                })
            }
        } else {
            const newKey = nanoid()
            const metadata = { contentType: 'image/*' }
            const uploadTask = firebase.storage().ref("Building/" + id + "/images/").child(arrayTest.name + '/').put(arrayTest, metadata)
            uploadTask.on(`state_changed`,
            (snapshot) => {
                let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                console.log('Upload is ' + progress + '% done')
                setErrorForm(false)
                setSendValid(true)
            },
            (error) => {
                console.log(error)
                setErrorForm(true)
                setSendValid(false)
            },
            // eslint-disable-next-line
            () => {
                firebase.storage().ref(`Building/${id}/images/${arrayTest.name}`).getDownloadURL().then(url => {
                    firebase.database().ref("Building/" + id + "/images/").child(newKey)
                    .set({

                        label,
                        buildingUrl:url,
                        isValid: true
                    })
                })
            })
        }
        }

    const handleCloseAlertStatus = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }
        setSendValid(false)
        setErrorForm(false)
    }

    return (
        <Container>
            <h1>Ajoute tes images</h1>
            <Link to={`/batiment/${id}`}>
                <ArrowBackIcon className={classes.returnIcon} />
            </Link>
            <form className={classes.createEditStyle}>
                <p>Entrez un nom</p>
                <TextField variant="outlined" className={classes.inputStyle} id="outlined-search" value={label} onChange={(e) => (setLabel(e.target.value))} label="Label" type="search" />
                <p>Vous pouvez chargez une ou plusieurs images</p>
                <label htmlFor="text-button-file">
                    <Button className={classes.buttonDisplayStyle} variant="contained" component="span">Upload Image</Button>
                </label>
                <h2>nombre de fichiers chargés : {lengthArray}</h2>
                <input
                    style={{ display:'none' }}
                    accept="image/*"
                    className={classes.input}
                    id="text-button-file"
                    multiple
                    type="file"
                    onChange={(e) => handleChange(e)}
                />
                <Button className={classes.inputStyle} variant="contained" color="primary" onClick={createImage}>Ajoutez Nouvelle image</Button>
            </form>
            <Snackbar open={errorForm} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="warning">Tout les champ sont requis !</Alert>
            </Snackbar>
            <Snackbar open={sendValid} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="success">Batiment créer avec succès</Alert>
            </Snackbar>
        </Container>
    )
}

export default CreateBuilding