import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import useStyles from '../assets/Stylebatiment'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Alert from '@material-ui/lab/Alert'
import { useParams } from 'react-router'
import firebase from 'firebase'
import nanoid from 'nanoid'
import {
    Container,
    Button,
    TextField,
} from '@material-ui/core'
import Snackbar from "@material-ui/core/Snackbar";


const AddJson = () => {
    
    const { id }    = useParams()
    const classes   = useStyles()
    const [errorForm, setErrorForm]       = useState(false)
    const [sendValid, setSendValid]       = useState(false)
    const [json, setJson]                 = useState(null)
    const [label, setLabel]               = useState('')


    const uploadJsonAnimation = () => {
        const newKey = nanoid()
        if (typeof json === 'object' && label) {
            const metadata = {
                contentType: 'application/json'
            }
            const uploadTask = firebase.storage().ref("Building/" + id + "/").child(json.name + '/').put(json, metadata)
            uploadTask.on(`state_changed`,
            (snapshot) => {
                let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
                console.log('Upload is ' + progress + '% done')
                console.log(snapshot)
                setErrorForm(false)
                setSendValid(true)
                console.log(typeof json)
            },
            (error) => {
                console.log(error)   
                setErrorForm(true)
                setSendValid(false)
            },
            () => {
                // si le nom change
                firebase.storage().ref(`Building/${id}/${json.name}`).getDownloadURL().then(url => {
                    firebase.database().ref("Building/" + id + "/").child(newKey)
                    .set({
                        label,
                        buildingUrl:url,
                        isValid: true
                    })
                })
            })
        }
    }

    const handleCloseAlertStatus = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSendValid(false)
        setErrorForm(false)
    }

    return (
        <Container>
            <h1>Ajoute un fichier Json</h1>
            <Link to={`/batiment/${id}`}>
                <ArrowBackIcon className={classes.returnIcon} />
            </Link>
            <form className={classes.createEditStyle}>
                <p>Entrez un nom</p>
                <TextField className={classes.inputStyle} variant="outlined" label="entre un label pour ce fichier Json" onChange={(e) => setLabel(e.target.value)} />
                <label htmlFor="text-button-file">
                    <Button  className={classes.buttonDisplayStyle} variant="contained" component="span">Upload Json</Button>
                </label>
                <p>Un seul fichier Json peut être chargé</p>
                <input
                    style={{ display:'none' }}
                    accept="application/json"
                    className={classes.input}
                    id="text-button-file"
                    multiple
                    type="file"
                    onChange={(e) => setJson(e.target.files[0])}
                />                
                <Button className={classes.inputStyle} variant="contained" color="primary" onClick={uploadJsonAnimation}>Validez l'ajout</Button>
            </form>
            <Snackbar open={errorForm} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="warning">
                    Tout les champ sont requis !
                </Alert>
            </Snackbar>
            <Snackbar open={sendValid} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="success">
                    JSON ajouté avec succès !
                </Alert>
            </Snackbar>
        </Container>
    )
}

export default AddJson 
