import React, { useEffect, useState } from 'react'
import firebase from 'firebase'
import lottie from 'lottie-web'
import { makeStyles } from '@material-ui/core/styles'
import {
     Card,
     CardActionArea,
     CardActions,
     CardContent,
     CardMedia,
     Button,
     Typography,
     Container
} from '@material-ui/core'


const useStyles = makeStyles({
  root: {
    maxWidth: '100%',
    backgroundColor: '#424242',
    color: 'white',
    textAlign: 'center'
  },
  media: {
    height: 750,
    width: '100%'
  },
  linkStyle: {
      color: 'white'
  }
})

const AnimationJson = ({id}) => {
     
  const classes = useStyles()
  const animationContainer  = React.createRef()
  const [urlAnimation, setUrlAnimation] = useState('')


useEffect(() => {
    firebase.storage().ref(`/Building/${id}`).child("data.json").getDownloadURL().then((url) => (setUrlAnimation(url)))
    lottie.loadAnimation({
      container: animationContainer.current,
      path: urlAnimation,
      loop: true,
      autoplay: true,
      renderer: 'svg'
    })
    // eslint-disable-next-line
},[urlAnimation])

  return (
    <Container>
      <Card className={classes.root}>
          <CardActionArea>
              <CardMedia className={classes.media}>
                  <div className={classes.media} ref={animationContainer}></div>
              </CardMedia>
              <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                      Animation
                  </Typography>
              </CardContent>
          </CardActionArea>
          <CardActions>
              <Button className={classes.linkStyle}  onClick={() => lottie.stop()} size="large" >
                  stop
              </Button>
              <Button className={classes.linkStyle} onClick={() => lottie.play()} size="large" >
                  Play
              </Button>
          </CardActions>
      </Card>
    </Container>
  )
}

export default AnimationJson