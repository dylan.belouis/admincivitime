import React, { useEffect, useState } from 'react'
import firebase from 'firebase'
import AnimationJson from './AnimationJson'
import SidePanel from '../../Home/SidePanel/SidePanel'
import useStyles from '../assets/Stylebatiment'
import { Link } from 'react-router-dom'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import CreateIcon from '@material-ui/icons/Create'
import Icon from '@material-ui/core/Icon'
import {
    CardMedia,
    CardContent,
    Typography,
    CardActions,
    Card,
    Container,
    Button
} from '@material-ui/core'

const PersonnalBuildingPage = (props) => {
    
    const classes   = useStyles()
    const { match } = props
    const { id }    = match.params
    const refImagesFolder     = firebase.database().ref(`Building/${id}/images`)
    const animationContainer  = React.createRef()
    const [imagesFolder, setImagesFolder] = useState([])
    const [dataInDatabase, setDataInDatabase] = useState(true)

    useEffect(() => {
        imagesFolder && refImagesFolder.on("value", function (snapshot) {
            if(snapshot.val() === undefined || snapshot.val() === null ) {
                setDataInDatabase(false)
            } else {
                setImagesFolder(Object.entries(snapshot.val()));

            }
        })
        // eslint-disable-next-line
    },[])

    return (
        <Container className={classes.NewBuilding}>
            <SidePanel />
            <Container>
                <Link to="/batiment">
                    <ArrowBackIcon className={classes.returnIcon} />
                </Link>
            </Container>
            <AnimationJson id={id}/>
            <Container className={classes.ButtonLinkAlign}>
                <Link to={`/batiment/${id}/create`}>
                    <Button variant="contained" color="primary" className={classes.button} startIcon={<Icon className={`fa fa-plus-circle`} />}>
                        Ajoutez fichiers images
                    </Button>
                </Link>
                <Link to={`/batiment/addjson/${id}/`}>
                    <Button variant="contained" color="primary" startIcon={<Icon className={`fa fa-plus-circle`} />} >
                        Ajoutez fichier JSON
                    </Button>
                </Link>
            </Container>
            <div ref={animationContainer}></div>
            { dataInDatabase ? <h1 className={classes.imageTextAlign}>Images de l'animation</h1> : <h1>Il n'y a pas d'images enregistrée en base !</h1> }
                <Container className={classes.ContainerGeneral}>
                { imagesFolder && imagesFolder.map(([key, batimentElem]) => (
                <Card className={classes.rootNewBuilding} key={key}>
                    <CardMedia>
                        <img src={batimentElem.buildingUrl} className={classes.imgStyle} alt="animation" />
                    </CardMedia>
                    <Container className={classes.displayTextButton}>
                        <CardContent className={classes.typoDisplay}>
                            <Typography gutterBottom component="h1">
                                label : {batimentElem.label}
                            </Typography>
                            <Typography component="h2">
                                { batimentElem.isValid ? ("Valide") : ("Non valide") }
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={`/batiment/edit/${id}/${key}/${batimentElem.isValid}`}>
                                <CreateIcon />
                            </Link>
                        </CardActions>
                    </Container>
                </Card>
            ))}
            </Container>
        </Container>
    )
}

export default PersonnalBuildingPage
