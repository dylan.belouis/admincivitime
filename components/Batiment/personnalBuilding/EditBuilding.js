import React, { useEffect, useState } from 'react'
import {
    Container,
    Button,
    TextField,

} from '@material-ui/core'
import { Link } from 'react-router-dom'
import useStyles from '../assets/Stylebatiment'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { useParams } from 'react-router'
import firebase from 'firebase'
import Alert from '@material-ui/lab/Alert'
import Snackbar from "@material-ui/core/Snackbar";


const EditBuilding = (props) => {

    const { id, key, valid }     = useParams()
    const refImagesFolder = firebase.database().ref(`Batiment/${id}/images/${key}`)
    const classes         = useStyles()
    const [imagesFolder, setImagesFolder]   = useState([])
    const [isValidState, setIsValidState]   = useState(valid)
    const [displayButton, setDisplayButton] = useState(true)
    const [errorForm, setErrorForm]         = useState(false)
    const [sendValid, setSendValid]         = useState(false)
    const [label, setLabel]                 = useState('')

    const updateBuilding = () => {
        if(label || isValidState) {
            firebase.database().ref(`Batiment/${id}/images`).child(key)
            .set({
                label: label,
                isValid: isValidState,
                buildingUrl: imagesFolder[0],
            })
            setSendValid(true)
            setErrorForm(false)
        } else {
            setErrorForm(true)
            setSendValid(false)
        }
    }

    const handleChange = (e) => {
        setLabel(e.target.value)
    }

    const handleClick = () => {
        setIsValidState(!isValidState)
        setDisplayButton(false)
    }

    const handleCloseAlertStatus = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }

        setSendValid(false)
        setErrorForm(false)
    }

    useEffect(() => {
        refImagesFolder.on("value", function (snapshot) {
            setImagesFolder(Object.values(snapshot.val()))
            console.log(valid)
        })
        // eslint-disable-next-line
    },[])

    return (
        <Container>
            <h1>Modifie le label ou la validité</h1>
            <Link to={`/batiment/${id}`}>
                <ArrowBackIcon className={classes.returnIcon} />
            </Link>
            <form className={classes.createEditStyle}>
                <p>Modifier le nom de l'image</p>
                <TextField  className={classes.inputStyle} variant="outlined" id="outlined-search" label={imagesFolder[2]} onChange={(e) => handleChange(e)} type="search" />
                <p>Modifier la validitée</p>
                <Button className={displayButton ? (classes.buttonDisplayStyle)  : (classes.buttonDisplayNone)} variant="contained" onClick={handleClick}>Passer à {imagesFolder[1] ? ("non valide") : ("valide")}</Button>
                <Button className={classes.inputStyle} variant="contained" color="primary" onClick={updateBuilding}>Validez la modification</Button>
            </form>
            <Snackbar open={errorForm} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="warning">
                    Erreur, modifier au moins un élément pour sauvegarder une modification
                </Alert>
            </Snackbar>
            <Snackbar open={sendValid} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="success">
                    Images modifiée avec succès
                </Alert>
            </Snackbar>
        </Container>
    )
}

export default EditBuilding