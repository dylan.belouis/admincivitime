import { makeStyles } from '@material-ui/core'


// styles
const useStyles = makeStyles({
    Batiment: {
        display: 'flex',
        justifyContent: 'center'
    },
    root: {
        width: '100%',
        textAlign: 'center',
        marginTop: '10%',
        backgroundColor: "#424242",
        color: "white"
    },
    imgStyle: {
        width: 245,
        maxHeight: 200,
    },
    rootNewBuilding: {
        width: 245,
        height: 345,
        display: 'flex',
        flexDirection: 'column',
        margin: 0,
        backgroundColor: '#F8E5E1',
        justifyContent: 'center'
    },
    imageTextAlign: {
        textAlign: 'center'
    },
    displayTextButton: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        textAlign: 'center',
        maxWidth: 245,
    },
    media: {
        margin: 'auto',
        height: 600,
        backgroundColor: "#424242",
        color: "white",
    },
    gridList: {
        width: '100%',
    },
    buttonCard: {
        display: 'flex',
        flexDirection: 'row',
        margin: 'auto'
    },
    eyesIcon: {
        fontSize: '50px',
        marginRight: '15px',
        color: 'white'
    },
    returnIcon: {
        fontSize: '50px'
    },
    FormStyle : {
        backgroundColor: 'white',
        display: 'flex',
        flexDirection: 'column',
    },
    colorTypoBuilding: {
        color: 'white'
    },
    ContainerGeneral: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        justifyContent: 'space-between'
    },
    NewBuilding: {
        width: '100vw',
        height: '100vh',
        position: 'relative'
    },
    createEditStyle: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center'
    },
    inputStyle: {
        marginBottom: 25,
        marginTop: 25,
        textAlign: 'center'
    },
    buttonDisplayStyle: {
        marginBottom: 25,
        marginTop: 25,
        alignItems: 'center',
        textAlign: 'center'
    },
    ButtonLinkAlign: {
        width: '100%',
        display: 'flex',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
        marginTop: 30
    },
    typoDisplay: {
        padding: 0,
        margin: 0
    },
    buttonAddImage: {
        marginTop: 10
    },
    buttonDisplayNone: {
        display: 'none',
        visibility: 'hidden'
    },
    buttonCreate: {
        display: 'flex',
        justifyContent: 'center',
        margin: 20
    }
})


  
  export default useStyles