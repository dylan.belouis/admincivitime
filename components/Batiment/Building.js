import React, { useState, useEffect } from 'react'
import firebase from 'firebase'
import { CardMedia, CardContent, Typography, Card, Button, Container, GridList, GridListTile, GridListTileBar } from '@material-ui/core'
import VisibilityIcon from '@material-ui/icons/Visibility'
import SidePanel from '../Home/SidePanel/SidePanel'
import Placeholder from './assets/placeholder.svg'
import useStyles from './assets/Stylebatiment'
import { Link } from 'react-router-dom'
import Icon from "@material-ui/core/Icon";

const Batiment = (props) => {
    const ref = firebase.database().ref("Building/")
    const [buildingData, setBuildingData] = useState([])
    const classes = useStyles()
    const [dataInDatabase, setDataInDatabase] = useState(true)

    


    useEffect(() => {
        ref.on("value", function (snapshot) {
            if(snapshot.val() === undefined || snapshot.val() === null) {
                setDataInDatabase(false)
            } else {
                setBuildingData(Object.entries(snapshot.val()))
            }
        })
        // eslint-disable-next-line
    }, [])

    return (
        <Container className="Batiment">
            <SidePanel />
            <Link  className={classes.buttonCreate} to={'/batiment/create'}>
                <Button variant="contained" color="primary" startIcon={<Icon className={`fa fa-plus-circle`} />}>
                    Créer un batiment
                </Button>
            </Link>
            { dataInDatabase ? <h1 className={classes.imageTextAlign}>Images de l'animation</h1> : <h1>Il n'y a pas encore de batiments, ajoutez en un !</h1> }
            <GridList cellHeight={180} className={classes.gridList}>
                {buildingData && buildingData.map(([key, building]) => (
                    <GridListTile key={key}>
                        { building.infosBatiment.buildingUrl ? ( <img src={building.infosBatiment.buildingUrl} alt={building.label} /> ) : (<h2>Problème de chargement de l'image</h2>) }
                        <GridListTileBar
                            title={building.infosBatiment.label}
                            actionIcon={
                                <Container className={classes.buttonCard}>
                                    <Link to={`/batiment/${key}`}>
                                        <VisibilityIcon className={classes.eyesIcon} /*onClick={() => openModal(key, building)} *//>
                                    </Link>
                                    <Link to={`/batiment/${key}/create`}>
                                        <Button className={classes.buttonAddImage} variant="contained">
                                            Ajoutez une image
                                        </Button>
                                    </Link>
                                </Container>
                                } 
                        />
                    </GridListTile>
                ))}
            </GridList>
        </Container>
    )
}

export default Batiment