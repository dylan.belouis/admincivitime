import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import useStyles from './assets/Stylebatiment'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import Alert from '@material-ui/lab/Alert'
import firebase from 'firebase'
import nanoid from 'nanoid'
import {
    Container,
    Button,
    TextField,
} from '@material-ui/core'
import Snackbar from "@material-ui/core/Snackbar";


const CreateBuilding = (props) => {

    const classes   = useStyles()
    const [sendValid, setSendValid] = useState(false)
    const [errorForm, setErrorForm] = useState(false)
    const [image, setImage] = useState('')
    const [label, setLabel] = useState('')
    const [imagePreview, setImagePreview] = useState('')

    const uploadFile = () => {
        if (image && label) {
            const metadata = {
                contentType: 'application/json'
            }
            const uidLinks = nanoid()
            const uploadTask = firebase.storage().ref(`Building/${uidLinks}/${image.name}`).put(image, metadata)
            uploadTask.on(`state_changed`,
                (snapshot) => {
                    console.log(snapshot)
                    setSendValid(true)
                    setErrorForm(false)
                },
                (error) => {
                    console.log(error)
                    setErrorForm(true)
                    setSendValid(false)
                },
                () => {
                    firebase.storage().ref(`Building/${uidLinks}/${image.name}`).getDownloadURL().then(url => {
                        firebase.database().ref("Building/" + uidLinks + "/").child("infosBatiment")
                            .set({
                                label,
                                buildingUrl: url,
                                isValid: true
                            })
                        setSendValid(true)
                        setErrorForm(false)
                    })
                    setImage("")
                    setLabel("")
                    setImagePreview("")
                })
        } else {
            setErrorForm(true)
        }
    }

    const PreviewFile = (e) => {
        setImage(e.target.files[0])
    }
    const handleCloseAlertStatus = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSendValid(false)
        setErrorForm(false)
    }

    return (
        <Container>
            <Container>
                <Link to="/batiment">
                    <ArrowBackIcon className={classes.returnIcon} />
                </Link>
            </Container>
            <h1>Créer votre batiment !</h1>
            <h3>Ici, vous pouvez créer un nouveau batiment, donner lui un nom, et entrez l'image qui représentera votre batiment. </h3>
            <form className={classes.FormStyle}>
                <TextField  className={classes.inputStyle} variant="outlined" placeholder="Entre le nom du batiment ici" id="edit" value={label} onChange={(e) => (setLabel(e.target.value))} />
                <label htmlFor="text-button-file">
                    <Button className={classes.buttonDisplayStyle} variant="contained" component="span">Ajouter une image</Button>
                </label>
                <input hidden
                       accept="image"
                       className={classes.input}
                       id="text-button-file"
                       type="file"
                       onChange={PreviewFile}
                />
                <h3>Vous ne pouvez mettre qu'une seul image.</h3>
                <Button className={classes.inputStyle} variant="contained" onClick={uploadFile} color="primary" endIcon={<i className="fas fa-paper-plane"></i>}>Sauvegarder</Button>
            </form>
            <Snackbar open={errorForm} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="warning">
                    Tout les champ sont requis !
                </Alert>
            </Snackbar>
            <Snackbar open={sendValid} autoHideDuration={6000} onClose={handleCloseAlertStatus}>
                <Alert onClose={handleCloseAlertStatus} severity="success">
                    Batiment créer avec succès
                </Alert>
            </Snackbar>
        </Container>
    )
}

export default CreateBuilding
