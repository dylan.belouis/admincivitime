import React from 'react';
import './Authentication.css';
import { TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from '@reach/router';

const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 0,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
        margin: '30px 0 0 0',
    },

});

const Authentication = (props) => {
    const classes = useStyles();
    return (
        <div className="Authentication">
            <div className="displaying_Form">
                <h1>Connection</h1>
                <div>
                    <TextField id="username" type="text" label="Nom" variant="outlined" />
                </div>
                <div className="margin_TextField--Authenticate">
                    <TextField id="username" type="password" label="Mot de passe" variant="outlined" />
                </div>
                <Link className="Style_Link--Authenticate" to="/home">
                    <Button className={classes.root}>Primary</Button>
                </Link>
            </div>
        </div>
    )
}

export default Authentication;
