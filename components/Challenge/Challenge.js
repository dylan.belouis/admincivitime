import React, { useEffect, useState} from "react";
import firebase from "firebase";
import useStyles from "./Style.js";
import {TextField, Button, Container, Table, TableBody, TablePagination, DialogContent, DialogTitle, Dialog, DialogContentText, DialogActions, TableCell, TableContainer, TableHead, TableRow, Paper, Snackbar } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import Icon from '@material-ui/core/Icon';
import Modal from '@material-ui/core/Modal';
import CreateIcon from '@material-ui/icons/Create';
import { Alert, AlertTitle } from '@material-ui/lab/';
import SidePanel from "../Home/SidePanel/SidePanel";
const isNumber = require('is-number');

const Challenge = () => {
  
  const classes = useStyles();
  const fb = firebase
  const [challenges, setChallenges] = useState([]);
  const [keySelected, setKeySelected] = useState('');
  
  const [durationSend, setDurationSend] = useState('');
  const [explanationSend, setExplanationSend] = useState('');
  const [rewardSend, setRewardSend] = useState('');
  const [themeSend, setThemeSend] = useState('');
  const [feedbackSend, setFeedbackSend] = useState('');
  const [labelSend, setLabelSend] = useState('');
  
  const [changeButton, setChangeButton] = useState(true);
  const [open, setOpen] = useState(false)
  const [isNumberError, setIsNumberError] = useState(false)
  const [durationError, setDurationError] = useState(false)
  const [editAndSaveContent, setEditAndSaveContent] = useState(false)
  const ref = fb.database().ref("/challenge/uid").orderByChild("label")
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [openConfirme, setOpenConfirme] = React.useState(false)
  const [dataInDatabase, setDataInDatabase] = useState(false)


  // Ouverture/Fermeture du formulaire
  const handleOpen = () => {
    setOpen(true);
    setChangeButton(true)
    setEditAndSaveContent(false)
    setDurationSend("")
    setExplanationSend("")
    setFeedbackSend("")
    setLabelSend("")
    setRewardSend("")
    setThemeSend("")
  };
  
  const handleClose = () => {
    setOpen(false);
    setEditAndSaveContent(false)
  };

  // Creation d'un défi
  const saveData = () => {
    if(isNumberError === false && durationError === false && durationSend && explanationSend && feedbackSend && labelSend && themeSend) {
    fb.database().ref("/challenge").child("/uid").push({
      duration: durationSend,
      explanation: explanationSend,
      reward : rewardSend,
      feedback: feedbackSend,
      theme: themeSend,
      label: labelSend
    })
    setOpen(false)
  } else {
    setEditAndSaveContent(true)
    }
  };

  //ouverture du formulaire d'édition
  const openEditForm = (id, challenge ) => {
    setKeySelected(id)
    setChangeButton(false)
    setOpen(true)
    setDurationSend(challenge.duration)
    setExplanationSend(challenge.explanation)
    setFeedbackSend(challenge.feedback)
    setLabelSend(challenge.label)
    setRewardSend(challenge.reward)
    setThemeSend(challenge.theme)
    setIsNumberError(false)
    setDurationError(false)
  }

  // Suppression d'un défi  
  const removeChallenge = () => {      
    fb.database().ref(`/challenge/uid/${keySelected}`).remove();
  }

  // Vérification des champs de valeur numérique duration
  const numberOrStringDuration = (e) => {
    setDurationSend(e.target.value)
    if(!isNumber(durationSend)){
      setDurationError(true)
    }
    if(isNumber(durationSend)){
      setDurationError(false)
    }
  } 

  // Vérification des champs de valeur numérique Reward
  const numberOrStringReward = (e) => {
    setRewardSend(e.target.value)
    if(!isNumber(rewardSend)){
      setIsNumberError(true)
    }
    if(isNumber(rewardSend)){
      setIsNumberError(false)
    }
  };

  // MAJ d'un défi
  const updateChallenge = () => {
    if(isNumberError === false && durationError === false && durationSend && explanationSend && feedbackSend && labelSend && themeSend) {
      setOpen(false)
      setIsNumberError(false)
      fb.database().ref(`/challenge/uid/${keySelected}`).set({
        duration: durationSend,
        explanation: explanationSend,
        reward : rewardSend,
        feedback: feedbackSend,
        theme: themeSend,
        label: labelSend
      })
    } else {
      setEditAndSaveContent(true)
    }
  }

  // Gestion Pagination
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };


  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleClickOpenConfirme = (id) => {
    setOpenConfirme(true);
    setKeySelected(id)
  };

  const handleCloseConfirme = () => {
    setOpenConfirme(false);
  }

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    setDataInDatabase(false);
  };

    
  // Gestion de la récupération des données
  useEffect(() => {
    ref.on("value", function(snapshot) {
      if(snapshot.val() === undefined || snapshot.val() === null) {
        setDataInDatabase(true)
      } else {
        setChallenges(Object.entries(snapshot.val()))
        setDataInDatabase(false)
      }
    })
    // eslint-disable-next-line
  },[])


return (
      <Container fixed className="Challenges">
        <SidePanel/>
        <div>
          <Container>
            <h1>Défis</h1>
          </Container>
        </div>
        <Paper>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="right">Duration</TableCell>
                  <TableCell align="right">Explanation</TableCell>
                  <TableCell align="right">Label</TableCell>
                  <TableCell align="right">Reward</TableCell>
                  <TableCell align="right">feedback</TableCell>
                  <TableCell align="right">Theme</TableCell>
                  <TableCell align="right">Delete</TableCell>
                  <TableCell align="right">Edit</TableCell>
                  <TableCell align="right">
                    <Icon onClick={handleOpen} className={`fa fa-plus-circle ${classes.IconSize}`} color="primary" />
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {challenges.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(([key, challenge]) => {
                  return (
                    <TableRow key={key} hover role="checkbox">
                      <TableCell align="right">
                        {challenge.duration}
                      </TableCell>
                      <TableCell align="right">
                        {challenge.explanation}
                      </TableCell>
                      <TableCell align="right">
                        {challenge.label}
                      </TableCell>
                      <TableCell align="right">
                        {challenge.reward}
                      </TableCell>
                      <TableCell align="right">
                        {challenge.feedback}
                      </TableCell>
                      <TableCell align="right">
                        {challenge.theme}
                      </TableCell>
                      <TableCell align="right">
                        <Button  onClick={() => handleClickOpenConfirme(key)} size="small" variant="contained" color="secondary"><DeleteIcon /></Button>
                      </TableCell>
                      <TableCell align="right">
                        <Button  onClick={() => openEditForm(key, challenge)} size="small" variant="contained"><CreateIcon /></Button>
                      </TableCell>
                    </TableRow>
                  );
                })} 
                <Dialog
                  open={openConfirme}
                  onClose={handleCloseConfirme}
                  aria-labelledby="alert-dialog-title"
                  aria-describedby="alert-dialog-description"
                >
                  <DialogTitle id="alert-dialog-title">{"Confirmation de supression"}</DialogTitle>
                  <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                      Êtes vous sur de vouloir supprimer ce défi ?
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleCloseConfirme} color="primary">
                      Annuler
                    </Button>
                    <Button onClick={removeChallenge} color="primary" autoFocus>
                      Supprimer
                    </Button>
                  </DialogActions>
                </Dialog>
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 25, 100]}
            component="div"
            count={challenges.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
        <Snackbar open={dataInDatabase} autoHideDuration={6000} onClose={handleCloseAlert}>
          <Alert onClose={handleCloseAlert} severity="warning">
            Il n'y a pas de données, créer un challenge !
          </Alert>
        </Snackbar>
        <div className={classes.backgroundModal}>
        <div>
        <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description" open={open} onClose={handleClose}>
          <div className={classes.settingDisplayForm}>
            <form id="form" className={classes.formDisplay} onSubmit={saveData}>
              <Button onClick={handleClose}>Retour</Button>
              <input type="hidden" name="uid" />
                <TextField id="filled-basic" label="duration" variant="filled"
                  type="text"
                  className="form-control"
                  onChange={numberOrStringDuration}
                  value={durationSend}
                />
                { durationError ? ( <Alert severity="warning"><AlertTitle>Warning</AlertTitle> Ici, c'est un chiffre ! Si vous souhaitez une durée null, entrez 0.</Alert> ) : (<></>) }
                <TextField id="filled-basic" label="explanation" variant="filled" 
                  type="text"
                  className="form-control"
                  onChange={e => setExplanationSend(e.target.value)}
                  value={explanationSend}
                />
                <TextField id="filled-basic" label="label" variant="filled"
                  type="text"
                  className="form-control"
                  onChange={e =>  setLabelSend(e.target.value)}
                  value={labelSend}
                />
                <TextField id="filled-basic" label="feedback"  variant="filled"
                  type="text"
                  className="form-control"
                  onChange={e => setFeedbackSend(e.target.value)}
                  value={feedbackSend}
                />
                <TextField id="filled-basic"  label="reward" variant="filled" 
                  type="text"
                  className="form-control"
                  onChange={numberOrStringReward}
                  value={rewardSend}
                />
                { isNumberError ? ( <Alert severity="warning"><AlertTitle>Warning</AlertTitle>Ici, c'est un chiffre !</Alert>) : (<></>) }
                <TextField id="filled-basic" label="theme" variant="filled" 
                  type="text"
                  className="form-control"
                  onChange={e => setThemeSend(e.target.value)}
                  value={themeSend}
                />
                { changeButton ? (
                <Button onClick={saveData} variant="contained" color="primary" className="btn btn-primary" startIcon={<SaveIcon />}>
                  Sauvegarder
                </Button>
                  ) : (
                <Button onClick={() => updateChallenge(keySelected)} variant="contained" color="primary" startIcon={<CreateIcon />}>
                  éditer
                </Button>
                )}
                { editAndSaveContent ? (  <Alert severity="error">
                                            <AlertTitle>Error</AlertTitle>
                                            Tout les champs doivent être validés
                                          </Alert> ) : ( <></> ) }
            </form>
          </div>
          </Modal>
        </div>
      </div>
    </Container>
  );
}


export default Challenge;
