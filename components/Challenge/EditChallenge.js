import React, {useEffect, useState} from 'react'
import firebase from "firebase";
import useStyles from "./Style.js";
import {TextField, Button, Modal} from '@material-ui/core';


const isNumber = require('is-number');

const EditChallenge = () => {

    const [durationSend, setDurationSend] = useState('');
    const [explanationSend, setExplanationSend] = useState('');
    const [rewardSend, setRewardSend] = useState('');
    const [themeSend, setThemeSend] = useState('');
    const [feedbackSend, setFeedbackSend] = useState('');
    const [labelSend, setLabelSend] = useState('');
    const [isNumberError, setIsNumberError] = useState(false)
    const [durationError, setDurationError] = useState(false)
    const [editAndSaveContent, setEditAndSaveContent] = useState(false)
    const ref = fb.database().ref("/challenge/uid").orderByChild("label")
    const fb = firebase
    const [challenges, setChallenges] = useState([]);
    const [keySelected, setKeySelected] = useState('');
    const classes = useStyles()
    const [open, setOpen] = useState(false)

      // Ouverture/Fermeture du formulaire
  const handleOpen = () => {
    setOpen(true);
    setChangeButton(true)
    setEditAndSaveContent(false)
    setDurationSend("")
    setExplanationSend("")
    setFeedbackSend("")
    setLabelSend("")
    setRewardSend("")
    setThemeSend("")
  };
  
  const handleClose = () => {
    setOpen(false);
    setEditAndSaveContent(false)
  };

      // Vérification des champs de valeur numérique Reward
  const numberOrStringReward = (e) => {
    setRewardSend(e.target.value)
    if(!isNumber(rewardSend)){
      setIsNumberError(true)
    }
    if(isNumber(rewardSend)){
      setIsNumberError(false)
    }
  };

    // MAJ d'un défi
    const updateChallenge = () => {
        if(isNumberError === false && durationError === false && durationSend && explanationSend && feedbackSend && labelSend && themeSend) {
          setIsNumberError(false)
          fb.database().ref(`/challenge/uid/${keySelected}`).set({
            duration: durationSend,
            explanation: explanationSend,
            reward : rewardSend,
            feedback: feedbackSend,
            theme: themeSend,
            label: labelSend
          })
        } else {
          setEditAndSaveContent(true)
        }
      }

        // Creation d'un défi
  const saveData = () => {
    if(isNumberError === false && durationError === false && durationSend && explanationSend && feedbackSend && labelSend && themeSend) {
    fb.database().ref("/challenge").child("/uid").push({
      duration: durationSend,
      explanation: explanationSend,
      reward : rewardSend,
      feedback: feedbackSend,
      theme: themeSend,
      label: labelSend
    })
    setOpen(false)
  } else {
    setEditAndSaveContent(true)
    }
  };

        // Gestion de la récupération des données
  useEffect(() => {
    ref.on("value", function(snapshot) {
    setChallenges(Object.entries(snapshot.val()));
    })
    // eslint-disable-next-line
  },[])

    


    return (
        <div className={classes.backgroundModal}>
        <div>
        <Modal aria-labelledby="simple-modal-title" aria-describedby="simple-modal-description" open={open} onClose={handleClose}>
          <div className={classes.settingDisplayForm}>
            <form id="form" className={classes.formDisplay} onSubmit={saveData}>
              <Button onClick={handleClose}>Retour</Button>
              <input type="hidden" name="uid" />
                <TextField id="filled-basic" label="duration" variant="filled"
                  type="text"
                  className="form-control"
                  onChange={numberOrStringDuration}
                  value={durationSend}
                />
                { durationError ? ( <Alert severity="warning"><AlertTitle>Warning</AlertTitle> Ici, c'est un chiffre ! Si vous souhaitez une durée null, entrez 0.</Alert> ) : (<></>) }
                <TextField id="filled-basic" label="explanation" variant="filled" 
                  type="text"
                  className="form-control"
                  onChange={e => setExplanationSend(e.target.value)}
                  value={explanationSend}
                />
                <TextField id="filled-basic" label="label" variant="filled"
                  type="text"
                  className="form-control"
                  onChange={e =>  setLabelSend(e.target.value)}
                  value={labelSend}
                />
                <TextField id="filled-basic" label="feedback"  variant="filled"
                  type="text"
                  className="form-control"
                  onChange={e => setFeedbackSend(e.target.value)}
                  value={feedbackSend}
                />
                <TextField id="filled-basic"  label="reward" variant="filled" 
                  type="text"
                  className="form-control"
                  onChange={numberOrStringReward}
                  value={rewardSend}
                />
                { isNumberError ? ( <Alert severity="warning"><AlertTitle>Warning</AlertTitle>Ici, c'est un chiffre !</Alert>) : (<></>) }
                <TextField id="filled-basic" label="theme" variant="filled" 
                  type="text"
                  className="form-control"
                  onChange={e => setThemeSend(e.target.value)}
                  value={themeSend}
                />
                { changeButton ? (
                <Button onClick={saveData} variant="contained" color="primary" className="btn btn-primary" startIcon={<SaveIcon />}>
                  Sauvegarder
                </Button>
                  ) : (
                <Button onClick={() => updateChallenge(keySelected)} variant="contained" color="primary" startIcon={<CreateIcon />}>
                  éditer
                </Button>
                )}
                { editAndSaveContent ? (  <Alert severity="error">
                                            <AlertTitle>Error</AlertTitle>
                                            Tout les champs doivent être validés
                                          </Alert> ) : ( <></> ) }
            </form>
          </div>
          </Modal>
        </div>
      </div>
    )
}

export default EditChallenge
