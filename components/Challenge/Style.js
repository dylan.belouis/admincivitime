import { makeStyles } from '@material-ui/core';


// Styles embarqué
const useStyles = makeStyles ({
    root: {
      backgroundColor: '#F3E4E1',
      width: '100%',
      display: 'flex',
      flexDirection: 'row'
    },
    root2: {
      backgroundColor: '#F3E4E1',
      width: '75%',
      display: 'flex',
      flexDirection: 'row',
      paddingRight: '25%'
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
    cardStyle: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      padding: '0 10% 0 10%',
      marginTop: '5%'
    },
    formDisplay: {
      margin: 'auto',
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      marginTop: '5%',
      backgroundColor: 'white',
      borderRadius: '5px',
    },
    settingDisplayForm: {
      margin: 'auto',
      display: 'flex',
      justifyContent: 'center',
      width: '50%'
    },
    addChallenge: {
      fontSize: '60px',
      width: '100%',
      cursor: 'pointer'
    },
    IconSize: {
      fontSize: '50px'
    },
    Challenges: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center'
    },
    backgroundModal: {
      display: 'flex',
      justifyContent: 'center',
    },
    displayButton: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center'
    },
    table: {
      minWidth: 650,
    },
    size: {
      width: '100%'
    },
  })
  
  export default useStyles;